#include "Display.h"
#include "Keypad.h"
#include "Menu.h"

LiquidCrystal lcd(8, 9, 4, 5, 6, 7);
Display display(lcd, 16, 2);
Keypad keypad;

void intValueChanged(int newValue)
{
  Serial.print("Int value changed: ");
  Serial.print(newValue);
  Serial.print("\n");
}

BaseMenuItem * subItems[] =
{
  new ActionMenuItem("Pod-akcja 1", 4),
  new ActionMenuItem("Pod-akcja 2", 5),
  new ActionMenuItem("Pod-akcja 3", 6),
  new IntMenuItem("Liczba", -10, 10, 5, intValueChanged),
  new BoolMenuItem("Przelacznik", true),
  new TimeMenuItem("Czas", 0, 600, 85)
};

BaseMenuItem * mainItems[] = 
{
  new SubMenuItem("\x01 Podmenu", subItems, sizeof(subItems) / sizeof(BaseMenuItem *)),
  new ActionMenuItem("Akcja 1", 1),
  new ActionMenuItem("Akcja 2", 2),
  new ActionMenuItem("Akcja 3", 3)
};

MainMenu mainMenu(display, mainItems, sizeof(mainItems) / sizeof(BaseMenuItem *));

void setup()
{
  Serial.begin(9600);
  while (!Serial);

  byte folder[8] = {
    B00000,
    B00000,
    B11000,
    B11111,
    B11111,
    B11111,
    B00000,
    B00000
  };
  
  lcd.createChar(1, folder);
  mainMenu.Show();
}
 
void loop()
{
  int key = keypad.ReadKey();
  int result = mainMenu.Action(key);
}
